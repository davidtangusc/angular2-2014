* .value() service api
* filters
	* orderBy
	* filter
	* custom filters with arguments
* Angular routing & single page apps
* creating custom directives
	* shared scope
		* student-grades element directive
	* isolate scope
		* grade-status-indicator attribute directive