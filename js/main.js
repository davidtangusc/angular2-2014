var app = angular.module('gradebook', ['ngRoute']);

app.config(function($routeProvider) {
  $routeProvider
    .when('/gradebook', {
      controller: 'GradebookController',
      templateUrl: '/js/templates/gradebook.html'
    })
    .when('/announcements', {
      templateUrl: '/js/templates/announcements.html'
    })
    .when('/roster', {
      templateUrl: '/js/templates/roster.html'
    })
    .otherwise({
      redirectTo: '/gradebook'
    });
});

app.value('toastr', toastr);

app.controller('GradebookController', function($scope, gradesApi, gradeStats, toastr) {
  //$http.get('grades.json').then(function(response) {
  //  console.log(response);
  //  $scope.students = response.data;
  //});
  $scope.students = [];

  gradesApi.all().then(function(response) {
    $scope.students = response.data;
  });

  $scope.$watch('students', function() {
    $scope.gradeAverage = gradeStats.getAverage($scope.students);
  }, true);

  $scope.addStudent = function() {
    $scope.students.push({
      first: $scope.first,
      last: $scope.last,
      grade: $scope.grade
    });

    $scope.first = null;
    $scope.last = null;
    $scope.grade = null;

    toastr.success('New student enrolled!');
  };
});

app.factory('gradesApi', function($http) {
  return {
    all: function() {
      return $http.get('grades.json');
    }
  }
});

app.factory('gradeStats', function() {
  function getAverage(students) {
    var sum = 0;

    students.forEach(function(student) {
      sum += student.grade;
    });

    return sum / students.length;
  }

  return {
    getAverage: getAverage
  }
});


app.filter('percentage', function() {
  return function(input, total, includeSymbol) {
    var percentage = (input / total) * 100;

    if (includeSymbol) {
      return percentage + '%';
    }

    return percentage;
  }
});


app.directive('studentGrades', function() {
  // DDO (directive definition object)
  return {
    restrict: 'E',
    templateUrl: '/js/templates/student-grades.html'
  };
});

// app.directive('gradeStatusIndicator', function() {
//   return {
//     restrict: 'A',
//     scope: {
//       gradeStatusIndicator: '@'
//     },
//     link: function($scope, $el, attr) {
//       console.log($scope.gradeStatusIndicator);
//       var score = $scope.gradeStatusIndicator;
//       var color;

//       if (score < 70) {
//         color = 'red';
//       } else if (score > 70 && score < 80) {
//         color = 'yellow';
//       } else {
//         color = 'green';
//       }

//       $el.css('background-color', color);
//     }
//   }
// });

app.directive('gradeStatusIndicator', function() {
  return {
    restrict: 'A',
    scope: {
      gradeStatusIndicator: '='
    },
    link: function($scope, $el, attr) {
      console.log($scope.gradeStatusIndicator);
      var score = $scope.gradeStatusIndicator;
      var color;

      if (score < 70) {
        color = 'red';
      } else if (score > 70 && score < 80) {
        color = 'yellow';
      } else {
        color = 'green';
      }

      $el.css('background-color', color);
    }
  }
});




